<div id="frontpage">
	<div id="frontpage-main">
		<h1 class="frontpage-title">
			Jeeves ERP
		</h1>
		<h2 class="frontpage-subtitle">
			Developer Documentation
		</h2>
	</div>
	<div id="frontpage-footer">
		<p>Updated: Jan 2016</p>
	</div>
</div>
