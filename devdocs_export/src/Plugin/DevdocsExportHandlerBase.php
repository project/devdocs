<?php

namespace Drupal\devdocs_export\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Devdocs export handler plugins.
 */
abstract class DevdocsExportHandlerBase extends PluginBase implements DevdocsExportHandlerInterface {

}
