Drupal Developer documentation
------------------------------

Helper module for Drupal developers. Provides some automation and export functionality.
Template files included.

* Use files in template directory as starting point for you documentation.
* Define Developer Documentation directory path at File system settings page admin/config/media/file-system
